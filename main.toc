\select@language {ngerman}
\contentsline {chapter}{\nonumberline Abbildungsverzeichnis}{2}{chapter*.3}
\contentsline {chapter}{\numberline {1}Theoretischer Hintergrund}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Maschinelles Lernen}{3}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Betreutes Lernen}{3}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Unbetreutes Lernen}{4}{subsection.1.1.2}
\contentsline {section}{\numberline {1.2}Neuronale Netzwerke}{4}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Abbildung nichtlinearer Funktionen}{5}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Feedforward Schritt auf Netzwerk}{5}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Training mit Backwards Propagation}{5}{subsection.1.2.3}
\contentsline {subsection}{\numberline {1.2.4}Optimierungsalgorithmen}{5}{subsection.1.2.4}
\contentsline {subsection}{\numberline {1.2.5}Kostenfunktionen?}{5}{subsection.1.2.5}
\contentsline {section}{\numberline {1.3}Autoencodernetzwerke}{5}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Techniken zur Regularisierung}{5}{subsection.1.3.1}
\contentsline {section}{\numberline {1.4}Datenregularisierung}{5}{section.1.4}
\contentsline {chapter}{\nonumberline Literaturverzeichnis}{6}{chapter*.4}
