\chapter{Ersatzmodellierung aerodynamischer Daten mit Modellen reduzierter Ordnung}
\label{chap:PODTPSAE}
Die in SMARTy implementierte Ersatzmodellierung auf Basis von POD und TPS basiert auf der Interpolation zwischen vorhandenen Trainingssnapshots in dem durch die POD Methode erzeugten Unterraum. Dabei wird die TPS Interpolationsmethode genutzt, um für die entsprechende Parameterkombination einen abweichenden Unterraumvektor zu generieren. Die Rekonstruktion des Unterraumvektors durch die POD Methode erzeugt eine näherungsweise Vorhersage für diese Parameterkombination. Im diesem Kapitel werden die POD und TPS Methoden beschrieben und wie sich der Vorgang durch die Nutzung von Autoencodernetzwerken zur Dimensionsreduzierung anstelle von POD ändert.

\section{Dimensionsreduzierung mit POD}
Proper Orthogonal Decomposition (POD), auch bekannt als PCA (Principal Component Analysis, dt.: Hauptkomponentenanalyse) ist eine Methode um einen Datensatz in orthogonale Hauptkomonenten zu zerlegen. Die Berechnung dieser Komponenten geschieht durch die Singulärwertzerlegung der Matrix des Datensatzes. Im Kontext der aerodynamischen Ersatzmodellierung besteht diese Matrix zeilenweise aus den $m$ Trainingssnapshots, aus welchen das Ersatzmodell gebildet wird. Bei diesen handelt es sich um Vektoren mit $n$ Elementen, dabei kann es sich etwa um Volumen oder Oberflächendruckwerte in definierter Reihenfolge handeln. Diese Snapshotmatrix, im folgenden $W \in \mathds{R}^{n \times m}$ wird mit einer Singulärwertzerlegung in zwei orthogonale Matritzen und eine Diagonalmatrix mit Singulärwerten zerlegt:

\begin{equation*}
    W = U \Sigma V^T
\end{equation*}

$U$ und $V$ sind orthogonale Matritzen mit den Größen $n \times n$ und $m \times m$. Bei $\Sigma \in \mathds{R}^{n \times m}$ handelt es sich um eine Diagonalmatrix der Singulärwerte $\sigma_1, \sigma_2, ...\sigma_n$. Diese sind alle $\geq 0 $ und kennzeichnen die Skalierung der jeweiligen Achsen im Datenset. Zusätzlich sind die Singulärwerte ihrer Größe nach absteigend geordnet, so dass $\sigma_1 \geq \sigma_2 \geq  ... \geq \sigma_{min(n, m)}$ gilt. Diese Tatsache kann für die Dimensionsreduzierung genutzt werden - Dabei werden lediglich die letzten $k$ Singulärwerte $\sigma_1 ... \sigma_k$ verwendet und alle überigen Werte auf $0$ gesetzt, wodurch die Dimension der Darstellung auf effektiv $k$ Elemente reduziert wird. \cite{mitSVD}, \cite{Weisstein2019svd}.

Durch die Singulärwertzerlegung hängt jeder einzelnen Snapshot $W_i$ direkt von einer Zeile der Matrix $V^T$ ab:
\begin{equation*}
    W^i = U\Sigma(V^T)^i 
\end{equation*}
Die Multiplikation mit der Diagonalmatrix $\Sigma$ mit dem Vektor $(V^T)^i$ lässt sich durch die folgende Summe über alle Elemente des Vektors, multipliziert mit den einzelnen Singulärwerten $\sigma$ abbilden:
\begin{equation}
    W_i =  \sum_{j=1}^{k} (\sigma_j V_i^j)U^j
    \label{eq:POD}
\end{equation}
Ein einzelner Snapshot hängt also von allen Elementen der Matrix $U$ ab, die Elemente dieser werden als POD Moden bezeichnet. $(\sigma_j V_i^j) = a_i^j$ wird als POD Koeffizient bezeichnet, der Vektor $a^i = (a_1^i, a_2^i \dots a_m^i)$ ist der Koeffizientenvektor, von welchem der Snapshot $i$ abhängt. Er besteht zwar aus $m$ Elementen, also der vollständigen Snapshotdimension, allerdings durch die Multiplikation mit den Singulärwerten maximal die ersten $n$ Werte dieses ungleich $0$, eine Dimensionsreduktion auf die ersten $k$ Singulärwerte reduziert den Koeffizientenvektor ebenfalls auf $k$ Werte ungleich $0$.

Da ein Snapshot direkt vom Koeffizientenvektor abhängt, kann das Verfahren für die Generierung einer Vorhersage außerhalb der Snapshotmatrix $W$ genutzt werden. Dabei wird mithilfe eines Interpolationsverfahrens, etwa mit der im Folgenden vorgestellten TPS Methode, ein neuer Koeffizientenvektor $a$ erzeugt. Für die Interpolationsmethode kann die Tatsache genutzt werden, dass für jeden Vektor $a^i$ der vorhandenen Snapshots $W^i$ eine Ausgangsparameterkombination bekannt ist. Diese Zuordnungen aus Berechnungsparametern zu POD Koeffizientenvektoren können als Stützstellen für eine Interpolationsmethode genutzt werden, welche es dann ermöglicht zwischen den bereits bekannten Parameterkombinationen neue Koeffizientenvektoren zu berechnen. \cite[Kapitel~3.3]{franz2016reduced}

\begin{figure}[tb]
    \centering
    \includegraphics[width=\textwidth]{podTPS/pca.png}
    \caption{Hauptkomponentenanalyse, eingezeichnet sind die beiden orthogonalen Hauptachsen des Datensets. Quelle: \cite{Nazrul2018Jul}}
    \label{fig:PCA}
\end{figure}

\FloatBarrier
\section{Interpolationsmethode TPS}
\label{sec:tps}
Für die Interpolation der Koeffizientenvektoren des POD Verfahrens ist in SMARTy die Thin Plate Spline (TPS) Methode implementiert. Die Methode wurde in dieser Arbeit nicht nur in Verbindung mit POD eingesetzt, sondern auch für die Interpolation innerhalb der latenten Repräsentation des Autoencodernetzwerks. 

TPS basiert darauf eine minimal gebogene Oberfläche durch eine Menge an Kontrollpunkten zu legen. Ein Beispiel für diese Methode ist in Abbildung \ref{fig:TPS} dargestellt, hier wird ein eindimensionaler Wert, welcher von einem zweidimensionalen Parametervektor abhängig ist, interpoliert.

TPS verfügt über einen Regularisierungsterm, welcher der Methode ermöglicht nicht sämtliche Kontrollpunkte genau abzubilden, sondern Abweichungen zugunsten einer geringeren Biegung der Oberfläche erlaubt. Dies ist vor allem bei verrauschten Daten mit eventuellen Ausreißern interessant und ermöglicht für diese eine glattere Oberfläche, welche idealerweise in der Lage ist die zugrundeliegende Struktur der Daten besser abzubilden. Diese Regularisierung ist durch einen Parameter $\lambda$ steuerbar, Werte gegen $0$ sorgen für eine vollständige Abbildung aller Kontrollpunkte, Werte Richtung $\infty$ resultieren in einer graden Ebene ohne jegliche Biegung. \cite{Donato_approximationmethods}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{podTPS/tps.png}
    \caption{Interpolationsoberfläche welche zweidimensionale Parameter auf einen skalaren Wert abbildet. Die Kontrollpunkte welche die Fläche definieren sind als schwarze Kreise gekennzeichnet. Quelle: \cite{GordonWetzstein2003Feb}}
    \label{fig:TPS}
\end{figure}


Ziel der Methode ist es, eine Abbildung von $m$ dimensionalen Parametern zu $n$ dimensionalen Vektoren zu bilden. Im Einsatz für die Ersatzmodellierung bedeutet dies die Abbildung von $m$ Parametern auf die auf $n$ Dimensionen reduzierte Snapshotdarstellung. Der Spline, welcher eine Ebene zwischen $p$ Kontrollpunkten $c_i, i \in [1..p$] der Dimensions $\mathds{R}^m$  mit den Werten $d_i \in \mathds{R}^n, i \in [1..p]$ ist durch folgende Gleichung definiert:
    \begin{equation}
        \label{eq:tps}
        \begin{split}
            f(x) = a_1 + x^T A_r + \sum_{i=1}^{p}{w_i U (|c_i-x_i|)}\\
            \text{mit der Radialen Basisfunktion } U(r) = \begin{cases}
                r^2 log (r) & r > 0 \\
                0           & r= 0
            \end{cases}
        \end{split}
    \end{equation}
    Dabei sind $A \in \mathds{R}^{(m+1) \times n}$ und $W \in \mathds{R}^{p \times n}$ Kontrollmatritzen der für die Interpolationsoberfläche, welche Lage und Krümmung der Ebene definieren. $a_1$ bezeichnet die erste Zeile von $A$ während $A_r$ den Rest der Matrix ohne diese Zeile bezeichnet. Die Kontrollmatritzen ergeben sich aus der Lösung des folgenden linearen Gleichungssystems:
    \begin{equation}
        \label{eq:tpsControl}
        \begin{bmatrix}
            K   & P \\
            P^T & 0 \\
        \end{bmatrix}
        \begin{bmatrix}
            W \\
            A \\
        \end{bmatrix} =
        \begin{bmatrix}
            V \\
            0 \\
        \end{bmatrix}
    \end{equation}
    Die Matrix $K \in \mathds{R}^{p \times p}$ besteht aus der, in \ref{eq:tps} definierten, Radialen Basis Funktion $U$ der euklidischen Distanzen zwischen den Elementen des Vektors der Kontrollpunkte $C$:
    \begin{equation*}
        K_{ij} = U(\|c_i - c_j \|)
    \end{equation*}
    Für den Regularisierungsparameter $\lambda \neq 0$ beinhaltet diese Matrix zusätzlich noch folgenden Regularisierungsterm welcher eine Abweichung der Oberfläche  von den Kontrollpunkten ermöglicht:
    \begin{equation*}
        \begin{split}
            K_{ij} = U(\|c_i - c_j \|) + \alpha ^2 \lambda \quad i,j \in [1..p]\\
            \text{mit } \alpha = \frac{1}{p} \sum_{i=1}^p \sum_{j = 1}^p \|c_i - c_j \|
        \end{split}
    \end{equation*}

$P \in \mathds{R}^{p\times(m+1)}$ beinhaltet die Kontrollpunkte $C$:

\begin{equation*}
    \label{eq:tpsPoints}
    P = \begin{bmatrix}
        1 & c_{11} & c_{12} & ... & c_{1m} \\ 
        1 & c_{21} & c_{22} & ... & c_{3m} \\ 
        1 & c_{31} & c_{22} & ... & c_{3m} \\ 
        ... \\
        1 & c_{p1} & c_{p2} & ... & c_{pm} \\ 

    \end{bmatrix}
\end{equation*}

und $V \in \mathds{R}^{p \times n}$ die Zugehörigen Kontrollwerte der jeweiligen Kontrollpunkte:
\begin{equation}
    \label{eq:tpsValues}
    V = \begin{bmatrix}
        d_{11} & d_{12} & .. & d_{1n} \\
        d_{21} & d_{22} & .. & d_{2n} \\
        d_{31} & d_{32} & .. & d_{2n} \\
        .. & .. & .. \\ 
        d_{p1} & d_{p2} & .. & d_{pn} \\
    \end{bmatrix}
\end{equation}


Die Lösungen $W$ und $A$ des Gleichungssystems in \ref{eq:tpsControl} definieren die Interpolationsoberfläche. Die Methode ist in SMARTy implementiert, wo für die Lösung des Gleichungssystems eine LU-Dekomposition \cite{Weisstein2019Aug} verwendet wird. \cite[Kapitel~3.1]{Loehndorf2017May}

Bei der Nutzung der Interpolationsmethode in Kombination mit POD sind die Kontrollwerte in Gleichung \ref{eq:tpsValues} durch die einzelnen Werte der in Gleichung \ref{eq:POD} definierten POD Koeffizienten $a_i, i \in [1..p]$ gegeben. Die Kontrollpunkte der in \ref{eq:tpsPoints} definierten Matrix sind die Ausgangsparameter der Snapshots, etwa Machzahlen und Anstellwinkel. Um nun eine neue Vorhersage zu treffen, wird die in \ref{eq:tps} definierte Funktion auf die Vorhersageparameter angewendet und damit ein neuer POD Koeffizientenvektor $a_{prediction}$ erzeugt. Dieser wird, wie in Gleichung \ref{eq:POD} definiert, mit dem bekannten POD Moden $U_j, \quad j \in [1..k]$ multipliziert, was den Vorhersagesnapshot ergibt.

\section{Kombination von TPS mit Autoencodernetzwerken}
In dieser Arbeit wurde die POD Methode durch ein Autoencodernetzwerk ersetzt und für die Vorhersage von neuen Snapshots mit der TPS Interpolationsmethode kombiniert, welche dabei im latenten Unterraum des Netzwerks interpoliert. Das Netzwerk wird in einer Offlinephase zunächst mit den vorhandenen Snapshots trainiert. Dabei ergibt sich folgneder Ablauf:

\begin{enumerate}
    \item Das gesamte Autoencodernetzwerk wird darauf trainiert, die gewünschte Strömungsgröße in den $m$ Snapshots $W_i, i \in [1..m]$ möglichst akkurat wiederzugeben, so dass \\ $f_{decoder}(f_{encoder}(W_i)) \approx W_i, i \in [1..m]$ gilt. Dieser Schritt entspricht in der ersetzten POD Methode der Singulärwertzerlegung, wobei Enocder und Decoderteil des Netzwerks, ähnlich der POD Moden $U$, über alle Snapshots verwendet werden und die latenten Vektoren, ähnlich den POD Koeffizienten $a_i$ jeweils einem Snapshot $W_i$ zugeordnet sind.
    \item Der Encoderteil des Netzwerks wird genutzt um für jeden Snapshot $W_i$ den latenten Vektor aus der mittleren Ebene des Autoencoders zu berechnen welcher sich durch $latent_i = f_{enocder}(W_i)$  ergibt. Da für jeden Snapshot Ausgangsparameter der Strömungsberechnung bekannt sind, ergibt sich, ähnlich wie bei den vorgestellten POD Koeffizienten, je Snapshot ein Paar von zusammenhängendem latenten und Parametervektor. 
    \item Mit diesem Paaren von Kontrollpunkten und Werten wird die Interpolationsmethode definiert. Im Fall von TPS bedeutet dies das Aufstellen und Lösen des in \ref{eq:tpsControl} beschriebenen Gleichungssystems, wobei die in \ref{eq:tpsValues} beschriebene Matrix die $m$ latenten Vektoren des Netzwerks beinhaltet. Dabei ergeben sich hier erneut die Matritzen $A \in \mathds{R}^{(m+1) \times n}$ und $W \in \mathds{R}^{p \times n}$, welche die Interpolationsoberfläche definieren.
\end{enumerate}
Damit ist der Offlineschritt der Methode abgeschlossen, womit die Kombination aus TPS und Autoencoder für Vorhersagen von Strömungsgrößen für neue Parameter genutzt werden kann. Dabei ergibt sich folgender Ablauf:
\begin{enumerate}
    \item Der TPS Interpolator berechnet für die gewünschte Parameterkombinationen einen latenten Vektor $latent_{prediction}$ mit der in \ref{eq:tps} definierten Gleichung. Dabei wird die im Offlineschritt zuvor berechneten Kontrollmatritzen $A$ und $W$ genutzt.
    \item Mit dem Dekoderteil des Netzwerks wird durch $W_{prediction} = f_{decoder}(latent_{prediction})$ ein Vorhersagesnapshot berechnet.
\end{enumerate}

